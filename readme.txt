Setup concourse with kite and bosh:

1. setup Docker environment for your workstation:
	- Mac OS: https://docs.docker.com/docker-for-mac/
	- Windows: https://docs.docker.com/docker-for-windows/

(1.1 setup bash completion: https://docs.docker.com/docker-for-mac/#installing-bash-completion )

2. create .env from .env.example and specify local path to create cloud config files in (lets say ~/myclouds/ )

3. $ docker-compose run --name mykitehelper -w /root kitehelper /bin/bash 

4. mykitehelper$ kite new /root/myclouds/bosh_concourse_cloud

5. $ exit

6. create ssh keys (mac os and linux): $ ssh-keygen -t rsa -f ~/.ssh/kite.key -N ''

7. Change keypaths in .env file if neccesary
 
8. edit ~/myclouds/bosh_concourse_cloud/config/cloud.yml
	- aws -> access_key and secret_key from AWS IAM User
	- bosh -> password (choose one)
	- bosh -> keypair name (with this name keypair will be added at aws)
	- bosh -> key settings: don't change this (change it in .env file)
	- bosh -> db_password (choose one)
	- concourse -> hostname will be created but domain must exist as zone in route 53
	- concourse -> dns_zone: zone id of the given domain
	- concourse -> choose auth credentials and db_password

9. start container again: $ docker start -ai mykitehelper

10. mykitehelper$ cd myclouds/bosh_concourse_cloud/
11. mykitehelper$ kite generate --cloud=aws

(now see README.md)

12. mykitehelper$ cd terraform/
13. mykitehelper$ terraform init
14. mykitehelper$ terraform plan

15. mykitehelper$ terraform apply

16. mykitehelper$ cd ..
17. mykitehelper$ source bin/setup-tunnel.sh
18. mykitehelper$ kite render manifest bosh --cloud=aws
19. mykitehelper$ bin/bosh-install.sh

20. (test with: mykitehelper$ bosh -e bosh-director env)

21. mykitehelper$ source ./bin/set-env.sh
22. (mykitehelper$ bosh -e bosh-director ur https://bosh.io/d/github.com/cloudfoundry/garden-runc-release )

23. mykitehelper$ bin/concourse-deploy.sh 

--------

Tear down everything:

1. start container again: $ docker start -ai mykitehelper

2. $ bosh -e bosh-director delete-deployment -d concourse
3. $ bosh delete-env deployments/bosh/bosh.yml \
  --state=config/state.json \
  --vars-store=config/creds.yml \
  --vars-file=deployments/bosh/bosh_vars.yml \
  --var-file private_key=~/.ssh/bosh.key \
  -o deployments/bosh/cpi.yml \
  -o deployments/bosh/jumpbox-user.yml
4. $ cd terraform/
5. $ terraform destroy
6. $ exit

7. $ docker rm mykitehelper
8. $ docker rmi kitehelper:0.0.1kite

--------

Update kite (from git repo):

1. (stop runnig Dockercontainer)

2. $ git pull
3. $ docker cp . mykitehelper:/root/kite
4. $ docker start -ai mykitehelper

5. mykitehelper$ gem uninstall kite
6. mykitehelper$ cd /root/kite
7. mykitehelper$ gem build kite.gemspec
8. mykitehelper$ gem install kite-0.0.7.gem
9. mykitehelper$ cd /
10. mykitehelper$ go on with setup 11. above 

--------

Visualice AWS setup:

1. (run docker container: $ docker start -ai mykitehelper )
2. apt-get install graphviz
3. cd /root/myclouds/bosh_concourse_cloud/terraform
4. terraform graph | dot -Tpng > graph.png
